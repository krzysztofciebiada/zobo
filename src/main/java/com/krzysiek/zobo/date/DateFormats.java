package com.krzysiek.zobo.date;

import com.krzysiek.zobo.pregnancy.PregnancyConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class DateFormats {

    public static String convertStringDateFromZodiacSignToStringDate(String zodiacDate) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        String zodiacDateWithCurrentYear = zodiacDate + "." + currentYear;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(zodiacDateWithCurrentYear);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(parsedDate);
    }

    public static String convertStringDateFromZodiacSignToStringDate_2(String zodiacDate) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        String zodiacDateWithCurrentYear = zodiacDate + "." + (currentYear + 1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date parsedDate = null;
        try {
            parsedDate = simpleDateFormat.parse(zodiacDateWithCurrentYear);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(parsedDate);
    }

    public static Date addDaysToDate(Date date, int addDays) {
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusDays(addDays);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date minusDaysFromDate(Date date, int daysAmout) {
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.minusDays(daysAmout);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date addYearToDate(Date date, int yearAmount) {
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusYears(yearAmount);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date formatStringToDate(String givenDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date parsedStringToDate = null;
        try {
            parsedStringToDate = simpleDateFormat.parse(givenDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsedStringToDate;
    }

//    public static Date addDays(){
//        Date birthDate = new Date();
//        Calendar cal = Calendar.getInstance();
//        System.out.println("-----");
//        System.out.println(cal.toString());
//        System.out.println("-----");
//        cal.add(Calendar.DATE, 290);
//        System.out.println(cal.toString());
//        System.out.println("-----");
//        return new Date();
//    }
}
