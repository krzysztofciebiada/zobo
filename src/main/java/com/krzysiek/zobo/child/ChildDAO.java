package com.krzysiek.zobo.child;

import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.zodiacSign.ZodiacSign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChildDAO {

    private ChildRepository childRepository;

    @Autowired
    public ChildDAO(ChildRepository childRepository) {
        this.childRepository = childRepository;
    }

    public void saveNewChild(Child child) {
        childRepository.save(child);
    }

    public List<Child> findChildByUser(User user) {
        return childRepository.findAllByParents(user);
    }

    public Child findChildByNameAndZodiacSign(String name, ZodiacSign zodiacSign) {
        return childRepository.findChildByNameAndZodiacSign(name,zodiacSign);
    }

    public List<Child> findAllChilds() {
        return childRepository.findAll();
    }

//    public List<Child> findAllUsersChilds() {
//        return childRepository.findAllChildByUser();
//    }


}
