package com.krzysiek.zobo.child;

public class ChildExistsException extends RuntimeException  {
    public ChildExistsException(String message) {
        super(message);
    }
}
