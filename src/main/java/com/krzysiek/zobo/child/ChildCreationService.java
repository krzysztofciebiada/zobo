package com.krzysiek.zobo.child;

import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.user.UserContexHolder;
import com.krzysiek.zobo.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChildCreationService {

    private ChildDAO childDAO;
    private UserContexHolder userContexHolder;
    private ChildDTOToChildConverter childDTOToChildConverter;
    private UserDAO userDAO;

    @Autowired
    private ChildCreationService(ChildDAO childDAO, UserContexHolder userContexHolder, ChildDTOToChildConverter childDTOToChildConverter, UserDAO userDAO) {
        this.childDAO = childDAO;
        this.userContexHolder = userContexHolder;
        this.childDTOToChildConverter = childDTOToChildConverter;
        this.userDAO = userDAO;
    }

    public void addNewChild(ChildCreationDTO childCreationDTO) {
        if (childExists(childCreationDTO)) {
            throw new ChildExistsException("Masz już dziecko o pdoanym imieniu i znaku zodiaku");
        }
        Child child = childDTOToChildConverter.convertDTOToChild(childCreationDTO);
        childDAO.saveNewChild(child);
    }

    private boolean childExists(ChildCreationDTO childCreationDTO) {
        User userLoggedIn = userDAO.findUserByEmail(userContexHolder.getUserLoggedIn());
        if (childDAO.findChildByNameAndZodiacSign(childCreationDTO.getName(),childCreationDTO.getZodiacSign()) != null) {
            return true;
        }
        return false;
    }
}
