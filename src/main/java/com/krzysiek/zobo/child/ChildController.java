package com.krzysiek.zobo.child;

import com.krzysiek.zobo.user.UserContexHolder;
import com.krzysiek.zobo.user.UserDAO;
import com.krzysiek.zobo.zodiacSign.ZodiacSign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ChildController {

    private ChildCreationService childCreationService;
    private ChildDAO childDAO;
    private UserContexHolder userContexHolder;
    private UserDAO userDAO;

    @Autowired
    private ChildController(ChildCreationService childCreationService, ChildDAO childDAO, UserContexHolder userContexHolder, UserDAO userDAO) {
        this.childCreationService = childCreationService;
        this.childDAO = childDAO;
        this.userContexHolder = userContexHolder;
        this.userDAO = userDAO;
    }

    @GetMapping("/child/add")
    private String showAddChildForm(Model model) {
        model.addAttribute("childCreationDTO", new ChildCreationDTO());
        model.addAttribute("zodiacSigns", ZodiacSign.values());
        return "childAddForm";
    }

    @PostMapping("/child/add")
    private String addNewChild(@ModelAttribute @Valid ChildCreationDTO childCreationDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "childAddForm";
        }
        childCreationService.addNewChild(childCreationDTO);
        return "redirect:/child/list";
    }

    @GetMapping("/child/list")
    private String showChildList(Model model) {
        model.addAttribute("childList", childDAO.findChildByUser(userDAO.findUserByEmail(userContexHolder.getUserLoggedIn())));
//        model.addAttribute("childList", childDAO.findAllChilds());
        model.addAttribute("parentName", userDAO.findUserByEmail(userContexHolder.getUserLoggedIn()));
        return "childList";
    }
}
