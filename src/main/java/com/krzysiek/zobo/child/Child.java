package com.krzysiek.zobo.child;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.zodiacSign.ZodiacSign;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Child {


    @Id
    @GeneratedValue
    @JsonIgnore
    private Integer id;
    private String name;
    private ZodiacSign zodiacSign;
    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "child_parent")
    private Set<User> parents = new HashSet<>();
    private Date supposedBirthDate;
    private Date supposedBirthDateMin;
    private Date supposedBirthDateMax;

    public Date getSupposedDateOfConception() {
        return supposedDateOfConception;
    }

    public void setSupposedDateOfConception(Date supposedDateOfConception) {
        this.supposedDateOfConception = supposedDateOfConception;
    }

    private Date supposedDateOfConception;

    public Set<User> getParents() {
        return parents;
    }

    public void setParents(Set<User> parents) {
        this.parents = parents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZodiacSign getZodiacSign() {
        return zodiacSign;
    }

    public void setZodiacSign(ZodiacSign zodiacSign) {
        this.zodiacSign = zodiacSign;
    }

    public Date getSupposedBirthDate() {
        return supposedBirthDate;
    }

    public void setSupposedBirthDate(Date supposedBirthDate) {
        this.supposedBirthDate = supposedBirthDate;
    }

    public Date getSupposedBirthDateMin() {
        return supposedBirthDateMin;
    }

    public void setSupposedBirthDateMin(Date supposedBirthDateMin) {
        this.supposedBirthDateMin = supposedBirthDateMin;
    }

    public Date getSupposedBirthDateMax() {
        return supposedBirthDateMax;
    }

    public void setSupposedBirthDateMax(Date supposedBirthDateMax) {
        this.supposedBirthDateMax = supposedBirthDateMax;
    }
}
