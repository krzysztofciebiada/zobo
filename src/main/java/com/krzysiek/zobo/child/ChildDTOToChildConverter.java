package com.krzysiek.zobo.child;

import com.krzysiek.zobo.date.DateFormats;
import com.krzysiek.zobo.pregnancy.PregnancyConstants;
import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.user.UserContexHolder;
import com.krzysiek.zobo.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ChildDTOToChildConverter {

    private UserDAO userDAO;
    private UserContexHolder userContexHolder;

    @Autowired
    public ChildDTOToChildConverter(UserDAO userDAO, UserContexHolder userContexHolder) {
        this.userDAO = userDAO;
        this.userContexHolder = userContexHolder;
    }

    public Child convertDTOToChild(ChildCreationDTO childCreationDTO) {
        Child child = new Child();

        child.setName(childCreationDTO.getName());
        child.setZodiacSign(childCreationDTO.getZodiacSign());
        setDatesOnChild(child,childCreationDTO);
        populateParentForChild(child);
        return child;
    }

    private void populateParentForChild(Child child) {
        child.getParents().add(userDAO.findUserByEmail(userContexHolder.getUserLoggedIn()));
    }

    //todo add method to set date to middle
    private void setDatesOnChild(Child child, ChildCreationDTO childCreationDTO) {
        Date date = DateFormats.formatStringToDate(
                        DateFormats.convertStringDateFromZodiacSignToStringDate(childCreationDTO.getZodiacSign().getStartDate()));
        child.setSupposedBirthDate(DateFormats.addYearToDate(date, 1));
        child.setSupposedDateOfConception(DateFormats.minusDaysFromDate(
                child.getSupposedBirthDate(),PregnancyConstants.PREGNANCY_LENGTH));
    }
}
