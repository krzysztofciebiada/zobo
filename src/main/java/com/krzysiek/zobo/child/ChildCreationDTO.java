package com.krzysiek.zobo.child;

import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.zodiacSign.ZodiacSign;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Service
public class ChildCreationDTO {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 25, message = "Imię powinno składać się z przynajmniej {min} i max {max} liter.")
    private String name;
    @NotNull(message = "Znak zodiaku musi zostać wybrany.")
    private ZodiacSign zodiacSign;
    private User parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZodiacSign getZodiacSign() {
        return zodiacSign;
    }

    public void setZodiacSign(ZodiacSign zodiacSign) {
        this.zodiacSign = zodiacSign;
    }

    public User getParent() {
        return parent;
    }

    public void setParent(User parent) {
        this.parent = parent;
    }
}
