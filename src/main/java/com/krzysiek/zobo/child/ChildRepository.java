package com.krzysiek.zobo.child;

import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.zodiacSign.ZodiacSign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChildRepository extends JpaRepository<Child, Integer> {

    @Query("SELECT c FROM Child c " +
            "WHERE c.parents = ?1")
    List<Child> findChildByUserLogin(User user);

    Child findChildByNameAndZodiacSign(String name, ZodiacSign zodiacSign);

    List<Child> findAll();

    List<Child> findAllByParents(User user);


//    @Query("SELECT * FROM Child c " +
//            "W")
//    List<Child> findAllUserChild();

//    @Query("SELECT * " +
//            "FROM child c " +
//            "JOIN child_parent cp " +
//            "ON c.id=cp.child_id " +
//            "JOIN user u " +
//            "ON u.id=cp.parents_id")
//    List<Child> findAllChildByUser();
}
