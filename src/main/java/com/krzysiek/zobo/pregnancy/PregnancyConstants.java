package com.krzysiek.zobo.pregnancy;

public class PregnancyConstants {

    public static final int PREGNANCY_LENGTH = 280;
    public static final int PREGNANCY_LENGTH_MIN = 260;
    public static final int PREGNANCY_LENGTH_MAX = 300;

}
