package com.krzysiek.zobo;

import com.krzysiek.zobo.user.User;
import com.krzysiek.zobo.user.UserContexHolder;
import com.krzysiek.zobo.user.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    private final UserContexHolder userContexHolder;
    private final UserDAO userDAO;

    @Autowired
    public MainController(UserContexHolder userContexHolder, UserDAO userDAO) {
        this.userContexHolder = userContexHolder;
        this.userDAO = userDAO;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        User user = userDAO.findUserByEmail(userContexHolder.getUserLoggedIn());
        model.addAttribute("user", user);
        return "index";
    }

}
