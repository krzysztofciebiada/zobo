package com.krzysiek.zobo.user;

import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Service
public class UserRegistrationDTO {

    @Email(message = "Format wpisanego email jest niepoprawny")
    private String login;
    @Size(min = 8, max = 30, message = "Hasło musi zawierać od {min} do {max} znaków")
    private String password;
    @NotBlank(message = "To pole nie może być puste")
    @NotNull(message = "To pole nie może być puste")
    @Size(max = 50, message = "Wyświetlana nazwa nie może być dłuższa niż 50 znaków")
    private String displayName;

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getDisplayName() {
        return displayName;
    }
}
