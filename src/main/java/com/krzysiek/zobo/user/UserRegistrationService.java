package com.krzysiek.zobo.user;

import com.krzysiek.zobo.role.Role;
import com.krzysiek.zobo.role.RoleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserRegistrationService {

    private UserDAO userDAO;
    private UserDTOToUserConverter userDTOToUserConverter;
    private RoleDAO roleDAO;

    @Autowired
    public UserRegistrationService(UserDAO userDAO, UserDTOToUserConverter userDTOToUserConverter, RoleDAO roleDAO) {
        this.userDAO = userDAO;
        this.userDTOToUserConverter = userDTOToUserConverter;
        this.roleDAO = roleDAO;
    }

    public void addNewUser(UserRegistrationDTO userRegistrationDTO) {
        if (userExists(userRegistrationDTO.getLogin())) {
            try {
                throw new UserExistsException("Użytkownik o podanym mailu już istnieje, podaj inny mail.");
            } catch (UserExistsException e) {
                e.printStackTrace();
            }
        }

        User user = userDTOToUserConverter.convertDTOToUser(userRegistrationDTO);
        final String roleName = "ROLE_USER";
        Role role = roleDAO.findUserByRoleName(roleName)
                .orElseGet(() -> roleDAO.addNewRole(roleName));
        Set<Role> roles = new HashSet<Role>();
        roles.add(role);
        user.setRolse(roles);

        userDAO.saveNewUser(user);
    }

    private boolean userExists(String login) {
        if (userDAO.findUserByEmail(login) != null) {
            return true;
        }
        return false;
    }

}
