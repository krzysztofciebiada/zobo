package com.krzysiek.zobo.user;

import com.krzysiek.zobo.child.ChildDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteService {

    private UserDAO userDAO;
    private ChildDAO childDAO;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserDeleteService(UserDAO userDAO, ChildDAO childDAO, PasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.childDAO = childDAO;
        this.passwordEncoder = passwordEncoder;
    }

    public void deleteUser(UserDeleteDTO userDeleteDTO) {
        User user = userDAO.findUserByEmail(userDeleteDTO.getLogin());
        if (user != null) {
            boolean b = passwordMatch(userDeleteDTO, user);
            System.out.println(b);
            if (b) {
                userDAO.deleteUser(user);
            }
        }
    }

    private boolean passwordMatch(UserDeleteDTO userDeleteDTO, User user) {
        if (passwordEncoder.encode(userDeleteDTO.getPassword()).equals(user.getPassword())) {
            return true;
        }
        return true;
    }
}
