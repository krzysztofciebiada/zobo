package com.krzysiek.zobo.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserRegistrationService userRegistrationService;
    private UserDeleteService userDeleteService;

    @Autowired
    private UserController(UserRegistrationService userRegistrationService, UserDAO userDAO, UserDeleteService userDeleteService) {
        this.userRegistrationService = userRegistrationService;
        this.userDeleteService = userDeleteService;
    }

    @GetMapping("/user/add")
    private String showAddNewUserForm(Model model) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "registerForm";
    }

    @PostMapping("/user/add")
    private String addNewUser(@ModelAttribute @Valid UserRegistrationDTO userRegistrationDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registerForm";
        }
        userRegistrationService.addNewUser(userRegistrationDTO);
        return "redirect:/";
    }

    @GetMapping("/user/login")
    private String showLoginForm() {
        return "loginForm";
    }

    @GetMapping("/user/delete")
    private String showDeleteUserForm(Model model) {
        model.addAttribute("userDeleteDTO", new UserDeleteDTO());
        return "deleteUserForm";
    }

    @PostMapping("/user/delete")
    private String deleteUserResult(@ModelAttribute UserDeleteDTO userDeleteDTO) {
        userDeleteService.deleteUser(userDeleteDTO);
        return "index";
    }
}
