package com.krzysiek.zobo.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.krzysiek.zobo.child.Child;
import com.krzysiek.zobo.role.Role;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class User {

    public User(){
    }

    @Id
    @GeneratedValue
    @JsonIgnore
    private Integer id;
    private String login;
    @JsonIgnore
    private String password;
    @JsonIgnore
    private String displayName;
    @ManyToMany
    @JoinTable(name = "user_role")
    @JsonIgnore
    private Set<Role> roles;
    @ManyToMany
    @JsonIgnore
    private Set<Child> childs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRolse(Set<Role> rolse) {
        this.roles = rolse;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Child> getChilds() {
        return childs;
    }

    public void setChilds(Set<Child> childs) {
        this.childs = childs;
    }
}
