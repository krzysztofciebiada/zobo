package com.krzysiek.zobo.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDAO {

    private UserRepository userRepository;

    @Autowired
    private UserDAO(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveNewUser(User user) {
        userRepository.save(user);
    }

    public User findUserByEmail(String login) {
        return userRepository.findUserByLogin(login);
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

}
