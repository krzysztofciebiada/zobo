package com.krzysiek.zobo.zodiacSign;

public enum ZodiacSign {

    VIRGO("Panna", "24.08", "22.09"),
    LIBRA("Waga", "23.09", "22.10"),
    SCORPIO("Skorpion", "23.10", "21.11"),
    SAGITTARIUS("Strzelec", "22.11", "21.12"),
    CAPRICORN("Koziorożec", "22.12", "19.01"),
    AQUARIUS("Wodnik", "20.01", "18.02"),
    PISCES("Ryby", "19.02", "20.03"),
    ARIES("Baran", "21.03", "19.04"),
    TAURUS("Byk", "20.04", "22.05"),
    GEMINI("Bliźnięta", "23.05", "21.06"),
    CANCER("Rak", "22.06", "22.07"),
    LEO("Lew", "23.07", "23.08");

    private String polishName;
    private String startDate;
    private String endDate;

    private ZodiacSign(String polishName, String startDate, String endDate) {
        this.polishName = polishName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    private ZodiacSign(String polishName) {
        this.polishName = polishName;
    }

    public ZodiacSign findZodiacSignAssignedToInputDate(){

        return ZodiacSign.ARIES;
    }

    public String getPolishName() {
        return polishName;
    }

    public void setPolishName(String polishName) {
        this.polishName = polishName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
